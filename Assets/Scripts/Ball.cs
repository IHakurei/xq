﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

	public Vector2 speed;

	private Vector2 iniSpeed;
	private Vector3 iniPos;
    private int dirrecio;
	// Use this for initialization
	void Start () 
	{
		iniSpeed = speed;
		iniPos = transform.position;
	}

	// Update is called once per frame
	void Update () 
	{
		transform.Translate (speed.x*Time.deltaTime, speed.y*Time.deltaTime, 0);
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Bounds") 
		{
			speed.y *= -1.15f;
		}
		if (other.tag == "Player")
        {
            dirrecio = Random.Range(-1, 1);
            if(dirrecio == 0)
            {
                while (dirrecio == 0)
                {
                    dirrecio = Random.Range(-1, 1);
                }
             
            }
            speed.x *= -1.20f;
            speed.y *= dirrecio;
		} 
		else if (other.tag == "Goal") 
		{
			Reset ();
		}
       else if (other.tag == "CPU")
        {
            speed.y *= -1.2f;
            
        }
	}

	void Reset()
	{
		transform.position = iniPos;
		speed = iniSpeed;
	}
}
