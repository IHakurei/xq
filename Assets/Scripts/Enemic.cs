﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemic : MonoBehaviour {

    public float velx;
    public Ball Ballpos;
    private Vector3 iniPos;
    private float move;
    public float maxx;
    private Vector3 tmpPos;


    void Awake()
    {
        iniPos = transform.position;
    }

    void Update()
    {
        if (Ballpos.transform.position.y < 0)
        {
            if (Ballpos.transform.position.x > transform.position.x)
            {
                move = 1;


            }
            else if (Ballpos.transform.position.x < transform.position.x)
            {
                move = -1;

            }

        }
        transform.Translate(move * velx * Time.deltaTime, 0, 0);

        if (transform.position.x >= maxx)
        {
            tmpPos = new Vector3(maxx, transform.position.y, transform.position.z);
            transform.position = tmpPos;
        }
        else if (transform.position.x >= maxx)
        {
            tmpPos = new Vector3(-maxx, transform.position.y, transform.position.z);
            transform.position = tmpPos;
        }

    }

}
