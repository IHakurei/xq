﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	private Transform playerTransform;
	private Vector3 newPos;
    private Vector3 tmpPos;
	public Transform ballPosition;
	public float speed;
	private float move;

	void Start()
	{
		// Equivalente a playerTransform = GetComponent<Transform> ();
		playerTransform = transform;

		newPos = playerTransform.position;
	}

	void Update () 
	{
        if (ballPosition.position.x > 0)
        {
            if (ballPosition.position.y > transform.position.y)
            {
                move = 1;
            }
            else if (ballPosition.position.y < transform.position.y)
            {
                move = -1;
            }
        }
		move *= Time.deltaTime;
		move *= speed;
		transform.Translate(0, move, 0);

		if (transform.position.y < -4.4f) {
			tmpPos= new Vector3 (transform.position.x, -4.4f, transform.position.z);
            transform.position = tmpPos;
		}else if(transform.position.y > 4.4f) {
			tmpPos = new Vector3 (transform.position.x, 4.4f, transform.position.z);
            transform.position = tmpPos;
        }
	}
}
